# Views Serialization Pager

## Table of contents

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers

## Introduction

The Views Serialization Pager module allows the user to configure a "Pagination" 
display of a serialization view.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. Visit
[Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

## Configuration

1. Navigate to Administration > Extend and enable the module and its
   dependencies.
2. Navigate to Administration > Structure > Views > Add view and create a
   new view of REST EXPORT.
3. In the Format field set, select the 'Serialization with Pager' style.
4. On the style settings, provide the Accepted request formats to use.
5. In the Pager field set, select the 'Full', item to display
6. Save the settings and view the result.

## Maintainers

- Manpreet Singh - [manpreet_singh](https://www.drupal.org/u/manpreet_singh)
- Pankaj Kumar  - [pankaj_lnweb](https://www.drupal.org/u/pankaj_lnweb)
- Shikha Dawar - [shikha_lnweb](https://www.drupal.org/u/shikha_lnweb)
