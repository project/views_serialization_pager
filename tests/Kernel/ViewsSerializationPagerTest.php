<?php

namespace Drupal\views_serialization_pager\Tests\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\rest\Plugin\views\display\RestExport;
use Drupal\views\Entity\View;
use Drupal\views\ViewExecutable;
use Drupal\views_serialization_pager\Plugin\views\style\ViewsSerializationPager;

/**
 * Tests for the ViewsSerializationPager style plugin.
 *
 * @group views_serialization_pager
 */
class ViewsSerializationPagerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'views',
    'views_serialization_pager',
    'serialization',
  ];

  /**
   * The serializer service.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  protected $serializer;

  /**
   * The View instance.
   *
   * @var \Drupal\views\ViewExecutable|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $view;

  /**
   * The RestExport display handler.
   *
   * @var \Drupal\rest\Plugin\views\display\RestExport|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $displayHandler;

  /**
   * The mock pager plugin instance.
   *
   * @var \Drupal\views\Plugin\views\pager\PagerPluginBase|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $pager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->view = $this->createMock(ViewExecutable::class);
    $this->view->result = [];

    $this->displayHandler = $this->createMock(RestExport::class);
    $this->displayHandler->method('getContentType')->willReturn('json');

    $this->pager = $this->getMockBuilder('Drupal\views\Plugin\views\pager\PagerPluginBase')
      ->disableOriginalConstructor()
      ->getMockForAbstractClass();
    $options = ['items_per_page' => 5, 'offset' => 1];
    $this->pager->init($this->view, $this->createMock('Drupal\views\Plugin\views\display\DisplayPluginBase'), $options);
    $this->pager->setItemsPerPage($options['items_per_page']);
    $this->pager->setCurrentPage(1);
    $this->pager->total_items = 10;

    $this->serializer = $this->container->get('serializer');
  }

  /**
   * Test for the render method of the ViewsSerializationPager plugin.
   */
  public function testRenderMethod() {
    // Create a mock view with pager.
    $view = $this->createMock(View::class);
    $view->result = $this->generateMockViewResults($this->pager->total_items);
    $view->pager = $this->pager;

    // Mock row plugin.
    $rowPlugin = $this->createMock('Drupal\\views\\Plugin\\views\\row\\RowPluginBase');
    $rowPlugin->method('render')->willReturnCallback(function ($row) {
      return ['id' => $row->id, 'title' => $row->title];
    });
    $view->rowPlugin = $rowPlugin;

    // Instantiate the ViewsSerializationPager plugin.
    $plugin = new ViewsSerializationPager([], 'dummy_serializer', [], $this->serializer, ['json', 'xml'], ['json' => 'serialization', 'xml' => 'serialization']);
    $plugin->options = ['formats' => ['xml', 'json']];
    $plugin->displayHandler = $this->displayHandler;
    $plugin->view = $view;

    // Call the render method and validate output.
    $output = $plugin->render();
    $decoded_output = json_decode($output, TRUE);

    // Validate the output structure.
    $this->validateRenderOutput($decoded_output, $this->pager->total_items);
  }

  /**
   * Generates mock view results.
   *
   * @param int $total_items
   *   The total number of items to generate.
   *
   * @return array
   *   The generated mock results.
   */
  private function generateMockViewResults($total_items) {
    $results = [];
    for ($i = 1; $i <= $total_items; $i++) {
      $results[] = (object) [
        'id' => $i,
        'title' => "Item $i",
      ];
    }
    return $results;
  }

  /**
   * Validates the rendered output.
   *
   * @param array $decoded_output
   *   The decoded output from the render method.
   * @param int $total_items
   *   The total number of items expected.
   */
  private function validateRenderOutput(array $decoded_output, $total_items) {
    $this->assertNotEmpty($decoded_output, 'Rendered output is not empty.');
    $this->assertArrayHasKey('rows', $decoded_output, 'Output contains rows key.');
    $this->assertArrayHasKey('pager', $decoded_output, 'Output contains pager key.');

    // Validate rows.
    $this->assertCount($total_items, $decoded_output['rows'], 'Correct number of rows are serialized.');

    // Validate row data.
    for ($i = 1; $i <= $total_items; $i++) {
      $this->assertEquals(
        ['id' => $i, 'title' => "Item $i"],
        $decoded_output['rows'][$i - 1],
        "Row $i matches expected data."
      );
    }

    // Validate pager.
    $this->assertEquals(1, $decoded_output['pager']['current_page'], 'Pager current page is correct.');
    $this->assertEquals(10, $decoded_output['pager']['total_items'], 'Pager total items is correct.');
    $this->assertEquals(5, $decoded_output['pager']['items_per_page'], 'Pager items per page is correct.');
  }

}
